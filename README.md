# PyMiner

[![Build Status](https://travis-ci.com/akizunx/TinyControl.svg?branch=master)](https://travis-ci.com/akizunx/TinyControl)
[![License](https://img.shields.io/badge/license-GPL-blue)](https://img.shields.io/badge/license-GPL-blue)
[![Stars](https://gitee.com/py2cn/pyminer/badge/star.svg?theme=gvp)](https://gitee.com/py2cn/pyminer/stargazers)
[![Platform](https://img.shields.io/badge/python-v3.8-blue)](https://img.shields.io/badge/python-v3.7-blue)
[![Platform](https://img.shields.io/badge/PyQt5-v5.15.0-blue)](https://img.shields.io/badge/PyQt5-v5.15.0-blue)

<center>
    <a src="https://img.shields.io/badge/QQ%e7%be%a4-orange">
        <img src="https://img.shields.io/badge/QQ%e7%be%a4-945391275-orange">
    </a>
    <a src="https://img.shields.io/badge/QQ-454017698-orange">
        <img src="https://img.shields.io/badge/QQ-454017698-orange">
    </a>
</center>

#### 简介
PyMiner一款基于数据工作空间的数学工具，通过加载插件的方式实现不同的需求，用易于操作的形式完成数学计算相关工作。



#### 技术说明

项目开发环境支持跨平台，（windows,linux,mac 都支持！如果使用出现问题，欢迎提issue），建议使用Python3.8+PyQt5.15进行开发。

#### Windows安装

```bash
#第一步：下载源代码
git clone https://gitee.com/py2cn/pyminer.git
#第二步：同步安装依赖包和PyMiner，如果遇到安装失败的情况需要手动安装
python -m pip install -i https://mirrors.cloud.tencent.com/pypi/simple -r requirements.txt
#第三步：运行主程序
python app2.py
```

#### Linux安装

```bash
#第一步：下载源代码
git clone https://gitee.com/py2cn/pyminer.git
#第二步：同步安装依赖包和PyMiner，如果遇到安装失败的情况需要手动安装
python3 -m pip install -i https://mirrors.cloud.tencent.com/pypi/simple -r requirements_linux.txt
#第三步：运行主程序
python3 app2.py
```


#### 联系我们

1.  作者：PyMiner Development Team
2.  邮箱：aboutlong@qq.com


#### 许可说明
本项目遵循GPL许可证。此外，对个人用户来说，本项目支持自由修改源码和使用，但是不支持任何未经授权许可的商用或其他盈利性行为，也不支持任何未经授权申请著作权的行为。如有违反以上许可，本项目管理团队有权进行否决。
许可解释权归属 PyMiner Development Team。

#### 预览

基本界面
![avatar](pyminer2/ui/source/screenshot/app2.png)

变量
![avatar](pyminer2/ui/source/screenshot/app2-table.png)

绘图
![avatar](pyminer2/ui/source/screenshot/app2-plot.png)

