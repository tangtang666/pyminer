import cgitb
import time
import logging
from pyminer2.pmappmodern import main

if __name__ == '__main__':
    t0 = time.time() # 开始时间
    # 异常处理设置
    cgitb.enable(format='text')

    # 日志设置
    logger = logging.getLogger(__name__)

    t2 = time.time() # 结束时间
    logging.info(f'time spent for importing modules {t2 - t0}')  # 打印日志，启动耗时
    main()
