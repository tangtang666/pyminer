<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>FormEditor</name>
    <message>
        <location filename="../ui/formeditor.ui" line="14"/>
        <source>Form</source>
        <translation>窗体</translation>
    </message>
    <message>
        <location filename="../ui/formeditor.ui" line="58"/>
        <source>UTF-8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/formeditor.ui" line="79"/>
        <source>Unix(LF)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/formeditor.ui" line="51"/>
        <source>Length:{0}  Lines:{1}</source>
        <translation>长度：{0}  行数：{1}</translation>
    </message>
    <message>
        <location filename="../ui/formeditor.ui" line="65"/>
        <source>Sel:{0} | {1}</source>
        <translation>选中：{0} | {1}</translation>
    </message>
    <message>
        <location filename="../ui/formeditor.ui" line="72"/>
        <source>Ln:{0}  Col:{1}</source>
        <translation>行：{0}  列：{1}</translation>
    </message>
</context>
<context>
    <name>PMCodeEditTabWidget</name>
    <message>
        <location filename="../tabwidget.py" line="255"/>
        <source>Open File</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="../tabwidget.py" line="368"/>
        <source>Run: %s</source>
        <translation>运行：%s</translation>
    </message>
</context>
<context>
    <name>PMCodeEditor</name>
    <message>
        <location filename="../editor.py" line="248"/>
        <source>Ln:1  Col:1</source>
        <translation>行：1  列：1</translation>
    </message>
    <message>
        <location filename="../editor.py" line="249"/>
        <source>Length:0  Lines:1</source>
        <translation>长度：0  行数：1</translation>
    </message>
    <message>
        <location filename="../editor.py" line="250"/>
        <source>Sel:0 | 0</source>
        <translation>选中：0 | 0</translation>
    </message>
    <message>
        <location filename="../editor.py" line="386"/>
        <source>Format Code</source>
        <translation>格式化代码</translation>
    </message>
    <message>
        <location filename="../editor.py" line="387"/>
        <source>Run Code</source>
        <translation>运行代码</translation>
    </message>
    <message>
        <location filename="../editor.py" line="388"/>
        <source>Run Selected Code</source>
        <translation>运行选中代码</translation>
    </message>
    <message>
        <location filename="../editor.py" line="427"/>
        <source>Ln:{0}  Col:{1}</source>
        <translation>行：{0}  列：{1}</translation>
    </message>
    <message>
        <location filename="../editor.py" line="436"/>
        <source>Length:{0}  Lines:{1}</source>
        <translation>长度：{0}  行数：{1}</translation>
    </message>
    <message>
        <location filename="../editor.py" line="447"/>
        <source>Sel:{0} | {1}</source>
        <translation>选中：{0} | {1}</translation>
    </message>
    <message>
        <location filename="../editor.py" line="504"/>
        <source>Save file</source>
        <translation>保存文件</translation>
    </message>
    <message>
        <location filename="../editor.py" line="532"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../editor.py" line="532"/>
        <source>Save file &quot;{0}&quot;?</source>
        <translation>保存文件 &quot;{0}&quot;？</translation>
    </message>
    <message>
        <location filename="../editor.py" line="569"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
</context>
</TS>
