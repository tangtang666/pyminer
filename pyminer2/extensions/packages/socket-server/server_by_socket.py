import socket
import threading
import json
from pyminer2.workspace.datamanager.datamanager import data_manager
import dill
import base64
import numpy
import pandas


class PMServer():
    extension_lib = None

    def __init__(self, port: int):
        data_manager.set_var('a', 123123123)
        s = socket.socket()  # 创建 socket 对象
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        host = '127.0.0.1'  # 获取本地主机名
        s.bind((host, port))  # 绑定端口
        s.listen(5)  # 等待客户端连接
        self.s = s
        self.dispatcher_dic = {'read_p': self.read_pickle_data,
                               'write_p': self.set_pickle_data}

    @staticmethod
    def pickle_decode_object(data_b64: str) -> object:
        try:
            print(base64.b64decode(data_b64.encode('ascii')))
            result = dill.loads(base64.b64decode(data_b64))
            return result
        except:
            import traceback
            traceback.print_exc()
            return None

    def pickle_encode_object(self, obj) -> str:
        data_seq = dill.dumps(obj)
        return base64.b64encode(data_seq).decode('ascii')

    def read_data(self, var_name: str) -> bytes:
        return var_name.encode('utf-8')

    def read_pickle_data(self, var_name: str) -> str:

        try:
            data = self.extension_lib.get_var(var_name)
            data_b64 = self.pickle_encode_object(data)
            response = json.dumps({var_name: data_b64, 'message': 'success'})
            return response
        except:
            pass
        return '{"message": "variable %s not found!"}' % var_name

    def set_pickle_data(self, var_name: str, data_b64: str, provider: str = 'server'):

        print(var_name, data_b64)
        data = PMServer.pickle_decode_object(data_b64)
        print(data)
        if data is not None:
            self.extension_lib.set_var(var_name, data)
            return json.dumps({"message": 'success'})
        return '{"message":"failed"}'

    def run(self):
        while True:
            p = b''
            c, addr = self.s.accept()
            while (1):
                b = c.recv(65536)
                p += b
                if b.endswith(b'PMEND'):
                    p = p.strip(b'PMEND')
                    break
            recv_dic = json.loads(p.decode('utf-8'))
            func = self.dispatcher_dic.get(recv_dic['method'])
            if func is not None:
                request_content = func(*recv_dic['params'])
                c.sendall(request_content.encode('utf-8')+b'PMEND')
            c.close()  # 关闭连接


def run_server(port: int, extension_lib):
    server = PMServer(port)
    server.extension_lib = extension_lib
    server.run()


def run(extension_lib):
    th = threading.Thread(target=run_server, args=(12306, extension_lib))
    th.setDaemon(True)
    th.start()