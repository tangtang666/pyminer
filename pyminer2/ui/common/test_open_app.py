import sys
import os
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtCore import QTimer

def f():
    a=''

    try:
        a+=1
    except:
        print('exception!!')
        import traceback
        traceback.print_exc()
if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    widget = QtWidgets.QWidget()
    widget.resize(800, 600)
    widget.setWindowTitle("Hello, PyQt5")
    widget.show()
    timer = QTimer()
    timer.start(100)
    timer.timeout.connect(f)
    sys.exit(app.exec_())